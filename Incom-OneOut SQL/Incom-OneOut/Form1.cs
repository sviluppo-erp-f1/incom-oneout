﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Incom_OneOut
{
    public partial class Form1 : Form
    {
        private XmlDocument docXml = new XmlDocument();
        private SAPbobsCOM.Company oCompanyFrom;
        private SAPbobsCOM.Company oCompanyTo;
        private SAPbobsCOM.SBObob oSBObob;
        private SAPbobsCOM.Recordset oRecordset;
        private SAPbobsCOM.Recordset oRecordset1;
        private SAPbobsCOM.Recordset oRecordset2;
        private SAPbobsCOM.Recordset oRecordset0;
        private SAPbobsCOM.Documents oOINV;
        private SAPbobsCOM.Documents oOPCH;
        private SAPbobsCOM.Documents oODPI;
        private SAPbobsCOM.Documents oODPO;
        private SAPbobsCOM.Documents oORIN;
        private SAPbobsCOM.Documents oORPC;
        private string DataRegistrazione;
        private DateTime DtRegistrazione;
        public Form1()
        {
            docXml.Load("parametriDB.xml");
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DtRegistrazione = DateTime.Parse(maskedTextBox1.Text.ToString());
            Elabora();
        }

        private void Elabora()
        {
            ScriviLog("Inizio importazione del: " + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss") + "");
            ConnectionSap1(docXml);
            ConnectionSap2(docXml);
            CercaDocumenti();
            DisconectSap();
            ScriviLog("Fine Importazioni");
        }

        private bool CercaDocumenti()
        {
            bool ret = true;
            int res = 0;
            oRecordset = oCompanyFrom.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            oRecordset.DoQuery("SELECT 	T0.\"DocEntry\" FROM \"OINV\" T0 INNER JOIN \"OCRD\" T1 ON T0.\"CardCode\" = T1.\"CardCode\" WHERE \"U_FO_EXPONEOUT\" = 'Y' AND T0.\"U_FO_IMPORTATA\" = 'N' ORDER BY T0.\"DocEntry\" ");
            while(!oRecordset.EoF)
            {
                if (CreaFattura(int.Parse(oRecordset.Fields.Item(0).Value.ToString())) != 0)
                {
                    return false;
                }
                oRecordset.MoveNext();
            }

            oRecordset.DoQuery("SELECT 	T0.\"DocEntry\" FROM \"ODPI\" T0 INNER JOIN \"OCRD\" T1 ON T0.\"CardCode\" = T1.\"CardCode\" WHERE \"U_FO_EXPONEOUT\" = 'Y' AND T0.\"U_FO_IMPORTATA\" = 'N' ORDER BY T0.\"DocEntry\" ");
            while (!oRecordset.EoF)
            {
                if (CreaFattAnticipo(int.Parse(oRecordset.Fields.Item(0).Value.ToString())) != 0)
                {
                    return false;
                }
                oRecordset.MoveNext();
            }

            oRecordset.DoQuery("SELECT 	T0.\"DocEntry\" FROM \"ORIN\" T0 INNER JOIN \"OCRD\" T1 ON T0.\"CardCode\" = T1.\"CardCode\" WHERE \"U_FO_EXPONEOUT\" = 'Y' AND T0.\"U_FO_IMPORTATA\" = 'N' ORDER BY T0.\"DocEntry\" ");
            while (!oRecordset.EoF)
            {
                if (CreaNotaCredito(int.Parse(oRecordset.Fields.Item(0).Value.ToString())) != 0)
                {
                    return false;
                }
                oRecordset.MoveNext();
            }

            return ret;
        }

        private int CreaFattura(int DocEntry)
        {
            int ErrorGetDati = 0;
            int res = 0;
            oRecordset1 = oCompanyTo.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            oRecordset0 = oCompanyFrom.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            oRecordset2 = oCompanyFrom.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            oOINV = oCompanyFrom.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInvoices);
            oOPCH = oCompanyTo.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseInvoices);

            //Recupero la fattura di Incom dal GetByKey
            oOINV.GetByKey(DocEntry);

            //Mi estraggo i dati per l'importazione dalla tabella utente di One Out
            oRecordset1.DoQuery("SELECT \"Name\" FROM \"@FO_PARIMPORT\" WHERE \"Code\"='CardCode' ");
            string CardCode = oRecordset1.Fields.Item(0).Value.ToString();
            if (CardCode == "")
            {
                ScriviLog("ERRORE, Business Partner non configurato su One Out");
                ErrorGetDati = 1;
            }
            else
            {
                oRecordset1.DoQuery("SELECT \"CardName\" FROM \"OCRD\" WHERE \"CardCode\"='" + CardCode + "' ");
                if (oRecordset1.Fields.Item(0).Value.ToString() == "")
                {
                    ScriviLog("ERRORE, Business Partner non trovato su One Out");
                    ErrorGetDati = 1;
                }
            }
            oRecordset1.DoQuery("SELECT \"Name\" FROM \"@FO_PARIMPORT\" WHERE \"Code\"='Serie' ");
            string SerieNome = oRecordset1.Fields.Item(0).Value.ToString();
            if (SerieNome == "")
            {
                ScriviLog("ERRORE, Serie di  Numerazione non configurata su One Out");
                ErrorGetDati = 1;
            }
            else
            {
                string Anno = oOINV.DocDate.ToString().Substring(8, 2);
                oRecordset1.DoQuery("SELECT \"Series\" FROM \"NNM1\" WHERE \"SeriesName\" = '" + SerieNome+Anno + "'");
                int SerieSap = 0;
                if (oRecordset1.Fields.Item(0).Value.ToString() != "0")
                {
                    SerieSap = int.Parse(oRecordset1.Fields.Item(0).Value.ToString());
                }
                else
                {
                    ScriviLog("ERRORE, Serie di  Numerazione non trovata su One Out");
                    ErrorGetDati = 1;
                }
            }

            if (ErrorGetDati == 0)
            {
                string CardCodeFrom = oOINV.CardCode;
                string NumAtCard = oOINV.DocNum.ToString();
                oOPCH.CardCode = CardCode;
                oOPCH.DocDate = DtRegistrazione;
                oOPCH.DocDueDate = oOINV.DocDueDate;
                oOPCH.VatDate = DtRegistrazione;
                oOPCH.TaxDate = oOINV.TaxDate;
                oOPCH.NumAtCard = NumAtCard;
                oOPCH.DocType = SAPbobsCOM.BoDocumentTypes.dDocument_Service;

                oRecordset0.DoQuery("SELECT SUM(\"LineTotal\"),\"AcctCode\", \"VatGroup\" FROM INV1  WHERE \"DocEntry\" = '" + DocEntry + "' GROUP BY \"AcctCode\", \"VatGroup\", \"DocEntry\"");
                while (!oRecordset0.EoF)
                {
                    if (ErrorGetDati == 0)
                    {
                        oRecordset1.DoQuery("SELECT \"U_U_FO_CONTO\" FROM OCRD WHERE \"CardCode\" = '" + CardCode + "'"); /////DA CAMBIARE IN U_FO_CONTO
                        string ContoCoge = oRecordset1.Fields.Item(0).Value.ToString();
                        if (ContoCoge == "")
                        {
                            //LOG CONTO COGE NON TROVATO
                            ScriviLog("ERRORE, Conto di Costo relativo al Business Partner: " + CardCode + " non trovato su One Out");
                            ErrorGetDati = 1;
                        }
                        oOPCH.Lines.AccountCode = ContoCoge;
                        oOPCH.Lines.UserFields.Fields.Item("U_XPdcomp").Value = oOINV.TaxDate;
                        oRecordset1.DoQuery("SELECT \"U_FO_PROGETTO\" FROM \"@FO_PROGETTI\" WHERE \"Name\" = '" + CardCodeFrom + "'");
                        string progetto = oRecordset1.Fields.Item(0).Value.ToString();
                        if (progetto == "")
                        {
                            ScriviLog("ERRORE, Progetto relativo al Business Partner: " + CardCodeFrom + " non trovato");
                            ErrorGetDati = 1;
                        }
                        oOPCH.Lines.ProjectCode = progetto;
                        //oOPCH.Lines.CostingCode = progetto;
                        oOPCH.Lines.Price = double.Parse(oRecordset0.Fields.Item(0).Value.ToString());
                        if (ErrorGetDati == 0)
                        {
                            oOPCH.Lines.Add();
                        }
                    }
                    oRecordset0.MoveNext();
                }
                
                if (ErrorGetDati == 0)
                {
                    res = oOPCH.Add();
                    if (res == 0)
                    {
                        //LOG IMPORTATA CON 
                        ScriviLog("Fattura Incom numero: "+NumAtCard+" importata con successo");
                        //Imposta fattura come importata
                        oRecordset2.DoQuery("UPDATE OINV SET \"U_FO_IMPORTATA\" = 'Y' WHERE \"DocEntry\" = '" + DocEntry + "'");
                    }
                    else
                    {
                        string err;
                        int code;
                        oCompanyTo.GetLastError(out code, out err);
                        ScriviLog("ERRORE, Importazione Fattura Incom numero: " + NumAtCard + " errore n: "+code+" - "+err);
                    }
                }
                else
                {
                    res = ErrorGetDati;
                }
                
            }
            return res;
        }

        private int CreaFattAnticipo(int DocEntry)
        {
            int ErrorGetDati = 0;
            int res = 0;
            oRecordset1 = oCompanyTo.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            oRecordset0 = oCompanyFrom.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            oRecordset2 = oCompanyFrom.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            oODPI = oCompanyFrom.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDownPayments);
            oODPO = oCompanyTo.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseDownPayments);

            //Recupero la fattura di Incom dal GetByKey
            oODPI.GetByKey(DocEntry);

            //Mi estraggo i dati per l'importazione dalla tabella utente di One Out
            oRecordset1.DoQuery("SELECT \"Name\" FROM \"@FO_PARIMPORT\" WHERE \"Code\"='CardCode' ");
            string CardCode = oRecordset1.Fields.Item(0).Value.ToString();
            if (CardCode == "")
            {
                ScriviLog("ERRORE, Business Partner non configurato su One Out");
                ErrorGetDati = 1;
            }
            else
            {
                oRecordset1.DoQuery("SELECT \"CardName\" FROM \"OCRD\" WHERE \"CardCode\"='" + CardCode + "' ");
                if (oRecordset1.Fields.Item(0).Value.ToString() == "")
                {
                    ScriviLog("ERRORE, Business Partner non trovato su One Out");
                    ErrorGetDati = 1;
                }
            }
            oRecordset1.DoQuery("SELECT \"Name\" FROM \"@FO_PARIMPORT\" WHERE \"Code\"='Serie' ");
            string SerieNome = oRecordset1.Fields.Item(0).Value.ToString();
            if (SerieNome == "")
            {
                ScriviLog("ERRORE, Serie di  Numerazione non configurata su One Out");
                ErrorGetDati = 1;
            }
            else
            {
                string Anno = oODPI.DocDate.ToString().Substring(8, 2);
                oRecordset1.DoQuery("SELECT \"Series\" FROM \"NNM1\" WHERE \"SeriesName\" = '" + SerieNome + Anno + "'");
                int SerieSap = 0;
                if (oRecordset1.Fields.Item(0).Value.ToString() != "0")
                {
                    SerieSap = int.Parse(oRecordset1.Fields.Item(0).Value.ToString());
                }
                else
                {
                    ScriviLog("ERRORE, Serie di  Numerazione non trovata su One Out");
                    ErrorGetDati = 1;
                }
            }

            if (ErrorGetDati == 0)
            {
                string CardCodeFrom = oODPI.CardCode;
                string NumAtCard = oODPI.DocNum.ToString();
                oODPO.CardCode = CardCode;
                oODPO.DocDate = DtRegistrazione;
                oODPO.DocDueDate = oODPI.DocDueDate;
                oODPO.VatDate = DtRegistrazione;
                oODPO.TaxDate = oODPI.TaxDate;
                oODPO.NumAtCard = NumAtCard;
                oODPO.DocType = SAPbobsCOM.BoDocumentTypes.dDocument_Service;

                oODPO.DownPaymentPercentage = oODPI.DownPaymentPercentage;
                oODPO.DownPaymentType = oODPI.DownPaymentType;
                oODPO.DownPaymentAmount = oODPI.DownPaymentAmount;

                oRecordset0.DoQuery("SELECT SUM(\"LineTotal\"),\"AcctCode\", \"VatGroup\" FROM DPI1  WHERE \"DocEntry\" = '" + DocEntry + "' GROUP BY \"AcctCode\", \"VatGroup\", \"DocEntry\"");
                while (!oRecordset0.EoF)
                {
                    if (ErrorGetDati == 0)
                    {
                        
                        oRecordset1.DoQuery("SELECT \"DpmClear\" FROM OCRD WHERE \"CardCode\" = '" + CardCode + "'"); 
                        string ContoCoge = oRecordset1.Fields.Item(0).Value.ToString();
                        if (ContoCoge == "")
                        {
                            //LOG CONTO COGE NON TROVATO
                            ScriviLog("ERRORE, Conto di Costo relativo al Business Partner: " + CardCode + " non trovato su One Out");
                            ErrorGetDati = 1;
                        }
                        oODPO.Lines.AccountCode = ContoCoge;
                        
                        oRecordset1.DoQuery("SELECT \"U_FO_PROGETTO\" FROM \"@FO_PROGETTI\" WHERE \"Name\" = '" + CardCodeFrom + "'");
                        string progetto = oRecordset1.Fields.Item(0).Value.ToString();
                        if (progetto == "")
                        {
                            ScriviLog("ERRORE, Progetto relativo al Business Partner: " + CardCodeFrom + " non trovato");
                            ErrorGetDati = 1;
                        }
                        oODPO.Lines.ProjectCode = progetto;
                        oOPCH.Lines.UserFields.Fields.Item("U_XPdcomp").Value = oOINV.TaxDate;
                        //oOPCH.Lines.CostingCode = progetto;
                        oODPO.Lines.Price = double.Parse(oRecordset0.Fields.Item(0).Value.ToString());
                        if (ErrorGetDati == 0)
                        {
                            oODPO.Lines.Add();
                        }
                    }
                    oRecordset0.MoveNext();
                }

                if (ErrorGetDati == 0)
                {
                    res = oODPO.Add();
                    if (res == 0)
                    {
                        //LOG IMPORTATA CON 
                        ScriviLog("Fattura Anticipo Incom numero: " + NumAtCard + " importata con successo");
                        //Imposta fattura come importata
                        oRecordset2.DoQuery("UPDATE ODPI SET \"U_FO_IMPORTATA\" = 'Y' WHERE \"DocEntry\" = '" + DocEntry + "'");
                    }
                    else
                    {
                        string err;
                        int code;
                        oCompanyTo.GetLastError(out code, out err);
                        ScriviLog("ERRORE, Importazione Fattura Anticipo Incom numero: " + NumAtCard + " errore n: " + code + " - " + err);
                    }
                }
                else
                {
                    res = ErrorGetDati;
                }

            }
            return res;
        }

        private int CreaNotaCredito(int DocEntry)
        {
            int ErrorGetDati = 0;
            int res = 0;
            oRecordset1 = oCompanyTo.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            oRecordset0 = oCompanyFrom.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            oRecordset2 = oCompanyFrom.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            oORIN = oCompanyFrom.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oCreditNotes);
            oORPC = oCompanyTo.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oPurchaseCreditNotes);

            //Recupero la fattura di Incom dal GetByKey
            oORIN.GetByKey(DocEntry);

            //Mi estraggo i dati per l'importazione dalla tabella utente di One Out
            oRecordset1.DoQuery("SELECT \"Name\" FROM \"@FO_PARIMPORT\" WHERE \"Code\"='CardCode' ");
            string CardCode = oRecordset1.Fields.Item(0).Value.ToString();
            if (CardCode == "")
            {
                ScriviLog("ERRORE, Business Partner non configurato su One Out");
                ErrorGetDati = 1;
            }
            else
            {
                oRecordset1.DoQuery("SELECT \"CardName\" FROM \"OCRD\" WHERE \"CardCode\"='" + CardCode + "' ");
                if (oRecordset1.Fields.Item(0).Value.ToString() == "")
                {
                    ScriviLog("ERRORE, Business Partner non trovato su One Out");
                    ErrorGetDati = 1;
                }
            }
            oRecordset1.DoQuery("SELECT \"Name\" FROM \"@FO_PARIMPORT\" WHERE \"Code\"='Serie' ");
            string SerieNome = oRecordset1.Fields.Item(0).Value.ToString();
            if (SerieNome == "")
            {
                ScriviLog("ERRORE, Serie di  Numerazione non configurata su One Out");
                ErrorGetDati = 1;
            }
            else
            {
                string Anno = oORIN.DocDate.ToString().Substring(8, 2);
                oRecordset1.DoQuery("SELECT \"Series\" FROM \"NNM1\" WHERE \"SeriesName\" = '" + SerieNome + Anno + "'");
                int SerieSap = 0;
                if (oRecordset1.Fields.Item(0).Value.ToString() != "0")
                {
                    SerieSap = int.Parse(oRecordset1.Fields.Item(0).Value.ToString());
                }
                else
                {
                    ScriviLog("ERRORE, Serie di  Numerazione non trovata su One Out");
                    ErrorGetDati = 1;
                }
            }

            if (ErrorGetDati == 0)
            {
                string CardCodeFrom = oORIN.CardCode;
                string NumAtCard = oORIN.DocNum.ToString();
                oORPC.CardCode = CardCode;
                oORPC.DocDate = DtRegistrazione;
                oORPC.DocDueDate = oORIN.DocDueDate;
                oORPC.VatDate = DtRegistrazione;
                oORPC.TaxDate = oORIN.TaxDate;
                oORPC.NumAtCard = NumAtCard;
                oORPC.DocType = SAPbobsCOM.BoDocumentTypes.dDocument_Service;

                oRecordset0.DoQuery("SELECT SUM(\"LineTotal\"),\"AcctCode\", \"VatGroup\" FROM RIN1  WHERE \"DocEntry\" = '" + DocEntry + "' GROUP BY \"AcctCode\", \"VatGroup\", \"DocEntry\"");
                while (!oRecordset0.EoF)
                {
                    if (ErrorGetDati == 0)
                    {
                        oRecordset1.DoQuery("SELECT \"U_U_FO_CONTO\" FROM OCRD WHERE \"CardCode\" = '" + CardCode + "'"); /////DA CAMBIARE IN U_FO_CONTO
                        string ContoCoge = oRecordset1.Fields.Item(0).Value.ToString();
                        if (ContoCoge == "")
                        {
                            //LOG CONTO COGE NON TROVATO
                            ScriviLog("ERRORE, Conto di Costo relativo al Business Partner: " + CardCode + " non trovato su One Out");
                            ErrorGetDati = 1;
                        }
                        oORPC.Lines.AccountCode = ContoCoge;

                        oRecordset1.DoQuery("SELECT \"U_FO_PROGETTO\" FROM \"@FO_PROGETTI\" WHERE \"Name\" = '" + CardCodeFrom + "'");
                        string progetto = oRecordset1.Fields.Item(0).Value.ToString();
                        if (progetto == "")
                        {
                            ScriviLog("ERRORE, Progetto relativo al Business Partner: " + CardCodeFrom + " non trovato");
                            ErrorGetDati = 1;
                        }
                        oORPC.Lines.ProjectCode = progetto;
                        //oOPCH.Lines.CostingCode = progetto;
                        oORPC.Lines.Price = double.Parse(oRecordset0.Fields.Item(0).Value.ToString());
                        oOPCH.Lines.UserFields.Fields.Item("U_XPdcomp").Value = oOINV.TaxDate;
                        if (ErrorGetDati == 0)
                        {
                            oORPC.Lines.Add();
                        }
                    }
                    oRecordset0.MoveNext();
                }

                if (ErrorGetDati == 0)
                {
                    res = oORPC.Add();
                    if (res == 0)
                    {
                        //LOG IMPORTATA CON 
                        ScriviLog("Nota di Credito Incom numero: " + NumAtCard + " importata con successo");
                        //Imposta fattura come importata
                        oRecordset2.DoQuery("UPDATE ORIN SET \"U_FO_IMPORTATA\" = 'Y' WHERE \"DocEntry\" = '" + DocEntry + "'");
                    }
                    else
                    {
                        string err;
                        int code;
                        oCompanyTo.GetLastError(out code, out err);
                        ScriviLog("ERRORE, Importazione Nota di Credito Incom numero: " + NumAtCard + " errore n: " + code + " - " + err);
                    }
                }
                else
                {
                    res = ErrorGetDati;
                }

            }
            return res;
        }

        private void ScriviLog(string Testo)
        {
            DateTime dt = new DateTime();
            dt = DateTime.Now;
            string logFile = "c:/temp/LogImportFatture_" + dt.ToString("yyyyMMdd") + ".txt";
            using (StreamWriter w = File.AppendText(logFile))
            {
                w.WriteLine(Testo);
            }
            Console.WriteLine(Testo);
            listBox1.Items.Add(Testo);
        }

        private void ConnectionSap1(XmlDocument docXml)
        {
            oCompanyFrom = new SAPbobsCOM.Company();
            string Database = "";
            //Leggo i parametri di connessione dal file xml
            XmlNodeList elemList = docXml.GetElementsByTagName("DB-From");
            foreach (XmlNode node in elemList)
            {
                oCompanyFrom.Server = node.Attributes["Server"].Value;
                //oCompany.LicenseServer = node.Attributes["License"].Value;
                oCompanyFrom.DbUserName = node.Attributes["DbUserName"].Value;
                oCompanyFrom.DbPassword = node.Attributes["DbPassword"].Value;
                oCompanyFrom.CompanyDB = node.Attributes["CompanyDB"].Value;
                Database = node.Attributes["CompanyDB"].Value;
                oCompanyFrom.UserName = node.Attributes["UserName"].Value;
                oCompanyFrom.Password = node.Attributes["Password"].Value;
            }
            //oCompanyFrom.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_HANADB;
            oCompanyFrom.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2017;
            oCompanyFrom.UseTrusted = false;

            int errCode = oCompanyFrom.Connect();

            if (errCode != 0)
            {
                string err;
                int code;
                oCompanyFrom.GetLastError(out code, out err);
                MessageBox.Show("CONNESSIONE A SAP: "+ Database +" NON RIUSCITA, errore N: " + code + "             " + err, "Errore");
                return;
            }
            else
            {
                ScriviLog("Connessione alla Società " + Database + " eseguita correttamente ");
            }
            oSBObob = oCompanyFrom.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoBridge);
        }

        private void ConnectionSap2(XmlDocument docXml)
        {
            oCompanyTo = new SAPbobsCOM.Company();
            string Database = "";
            //Leggo i parametri di connessione dal file xml
            XmlNodeList elemList = docXml.GetElementsByTagName("DB-To");
            foreach (XmlNode node in elemList)
            {
                oCompanyTo.Server = node.Attributes["Server"].Value;
                //oCompany.LicenseServer = node.Attributes["License"].Value;
                oCompanyTo.DbUserName = node.Attributes["DbUserName"].Value;
                oCompanyTo.DbPassword = node.Attributes["DbPassword"].Value;
                oCompanyTo.CompanyDB = node.Attributes["CompanyDB"].Value;
                Database = node.Attributes["CompanyDB"].Value;
                oCompanyTo.UserName = node.Attributes["UserName"].Value;
                oCompanyTo.Password = node.Attributes["Password"].Value;
            }

            //oCompanyTo.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_HANADB;
            oCompanyTo.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2017;
            oCompanyTo.UseTrusted = false;

            int errCode = oCompanyTo.Connect();

            if (errCode != 0)
            {
                string err;
                int code;
                oCompanyTo.GetLastError(out code, out err);
                MessageBox.Show("CONNESSIONE A SAP: " + Database + " NON RIUSCITA, errore N: " + code + "             " + err, "Errore");
                return;
            }
            else
            {
                ScriviLog("Connessione alla Società " + Database + " eseguita correttamente ");
            }
            oSBObob = oCompanyTo.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoBridge);
        }

        private void DisconectSap()
        {
            oCompanyFrom.Disconnect();
            oCompanyFrom = null;

            oCompanyTo.Disconnect();
            oCompanyTo = null;
            ScriviLog("Disconnesso da SAP ");
        }
    }
}
